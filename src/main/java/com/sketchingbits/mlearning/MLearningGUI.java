package com.sketchingbits.mlearning;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class MLearningGUI extends javax.swing.JFrame {
	     
	private static final long serialVersionUID = -3055028660972933235L;

    private javax.swing.JLabel celsiusLabel;
    private javax.swing.JButton convertButton;
    private javax.swing.JLabel fahrenheitLabel;
    private javax.swing.JTextField tempTextField;

		/** Creates new form CelsiusConverterGUI */
	    public MLearningGUI() {
	        initComponents();
	    }

	    public JMenuBar createMenuBar() {
	        JMenuBar menuBar = new JMenuBar();

	        JMenu menu = new JMenu("A Menu");
	        menu.setMnemonic(KeyEvent.VK_A);
	        menuBar.add(menu);
	 
	        return menuBar;
	    }
	    
	    private void initComponents() {
	    	this.setTitle("MLearning GUI");
	    	this.setJMenuBar(createMenuBar());
	    }
	    	 
	    /**
	     * @param args the command line arguments
	     */
	    public static void main(String args[]) {
	    	
	    	// Mac Properties
	    	System.setProperty("apple.laf.useScreenMenuBar", "true");
	    	System.setProperty("com.apple.mrj.application.apple.menu.about.name", "MLearning");
	    	 
	        java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                new MLearningGUI().setVisible(true);
	            }
	        });
	    }
}
