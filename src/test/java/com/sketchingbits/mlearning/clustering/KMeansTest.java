package com.sketchingbits.mlearning.clustering;

import java.util.List;

import org.junit.*;

public class KMeansTest {

	@Test
	public void simpleLinearClustering() {
		double[][] trainingExamples = { {0.0, 0.2, 0.8, 0.7},
										{0.2, 0.0, 0.9, 0.8},
										{0.5, 0.4, 0.1, 0.1},
										{0.8, 0.9, 0.0, 0.1},
										{0.7, 0.8, 0.1, 0.0}};
		
		KMeans kMeansAnalysis = new KMeans(trainingExamples,false,1,2);
		List<KMeans.Centroid> result = kMeansAnalysis.run();
		
		// Training examples 0 and 1 should be in Cluster 0
		Assert.assertEquals(new Integer(0), result.get(0).trainingExamples.get(0));
		Assert.assertEquals(new Integer(1), result.get(0).trainingExamples.get(1));
		
		// Training examples 2,3 and 4 should be in Cluster 1
		Assert.assertEquals(new Integer(2), result.get(1).trainingExamples.get(0));
		Assert.assertEquals(new Integer(3), result.get(1).trainingExamples.get(1));
		Assert.assertEquals(new Integer(4), result.get(1).trainingExamples.get(2));
	}

	@Test
	public void simpleClassificationClustering() {
		double[][] trainingExamples = { {0.0, 1.0, 1.0, 1.0},
										{0.0, 0.0, 0.0, 1.0},
										{1.0, 1.0, 1.0, 1.0},
										{1.0, 0.0, 0.0, 1.0},
										{1.0, 1.0, 1.0, 1.0}};
		
		KMeans kMeansAnalysis = new KMeans(trainingExamples,true,1,2);
		List<KMeans.Centroid> result = kMeansAnalysis.run();
		
		// Training examples 0 and 1 should be in Cluster 0
		Assert.assertEquals(new Integer(1), result.get(0).trainingExamples.get(0));
		Assert.assertEquals(new Integer(3), result.get(0).trainingExamples.get(1));
		
		// Training examples 2,3 and 4 should be in Cluster 1
		Assert.assertEquals(new Integer(0), result.get(1).trainingExamples.get(0));
		Assert.assertEquals(new Integer(2), result.get(1).trainingExamples.get(1));
		Assert.assertEquals(new Integer(4), result.get(1).trainingExamples.get(2));
	}
}
